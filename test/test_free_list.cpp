#include "allocator/free_list.hpp"
#include <catch2/catch.hpp>
#include <cstdint>

namespace { // unnamed
    struct object
    {
        int a, b, c;
    };

    struct big_object
    {
        std::uint64_t a, b, c;
    };
} // namespace

TEST_CASE("free_list", "[free_list]")
{
    REQUIRE(sizeof(object) == 12);
    REQUIRE(sizeof(big_object) == 24);

    SECTION("getters")
    {
        {
            free_list fl(sizeof(object));
            // node size is increased to 16 so that it is 8-byte aligned
            REQUIRE(fl.node_size() == sizeof(object) + 4);
            REQUIRE(fl.capacity_left() == 0);
            REQUIRE(fl.used() == 0);
            REQUIRE(fl.empty());

            constexpr std::size_t num_objs = 5;
            void* mem = std::malloc(fl.node_size() * num_objs);
            fl.insert(mem, fl.node_size() * num_objs);
            REQUIRE(fl.capacity_left() == num_objs);
            REQUIRE(fl.used() == 0);
            REQUIRE_FALSE(fl.empty());
            std::free(mem);
        }

        {
            free_list fl(sizeof(big_object));
            REQUIRE(fl.node_size() == sizeof(big_object));
            REQUIRE(fl.capacity_left() == 0);
            REQUIRE(fl.used() == 0);
            REQUIRE(fl.empty());
            constexpr std::size_t num_objs = 5;
            void* mem = std::malloc(sizeof(big_object) * num_objs);
            fl.insert(mem, sizeof(big_object) * num_objs);
            REQUIRE(fl.capacity_left() == num_objs);
            REQUIRE(fl.used() == 0);
            REQUIRE_FALSE(fl.empty());
            std::free(mem);
        }
    }

    SECTION("move ctor")
    {
        free_list fl1(sizeof(object));
        // node size is increased to 16 so that it is 8-byte aligned
        REQUIRE(fl1.node_size() == sizeof(object) + 4);
        REQUIRE(fl1.capacity_left() == 0);
        REQUIRE(fl1.used() == 0);
        REQUIRE(fl1.empty());

        constexpr std::size_t num_objs = 5;
        void* mem = std::malloc(fl1.node_size() * num_objs);
        fl1.insert(mem, fl1.node_size() * num_objs);
        REQUIRE(fl1.capacity_left() == num_objs);
        REQUIRE(fl1.used() == 0);
        REQUIRE_FALSE(fl1.empty());

        free_list fl2(std::move(fl1));
        REQUIRE(fl2.node_size() == sizeof(object) + 4);
        REQUIRE(fl2.capacity_left() == num_objs);
        REQUIRE(fl2.used() == 0);
        REQUIRE_FALSE(fl2.empty());

        std::free(mem);
    }

    SECTION("move assignment")
    {
        free_list fl1(sizeof(object));
        // node size is increased to 16 so that it is 8-byte aligned
        REQUIRE(fl1.node_size() == sizeof(object) + 4);
        REQUIRE(fl1.capacity_left() == 0);
        REQUIRE(fl1.used() == 0);
        REQUIRE(fl1.empty());

        constexpr std::size_t num_objs = 5;
        void* mem = std::malloc(fl1.node_size() * num_objs);
        fl1.insert(mem, fl1.node_size() * num_objs);
        REQUIRE(fl1.capacity_left() == num_objs);
        REQUIRE(fl1.used() == 0);
        REQUIRE_FALSE(fl1.empty());

        free_list fl2(sizeof(big_object));
        REQUIRE(fl2.node_size() == sizeof(big_object));
        REQUIRE(fl2.capacity_left() == 0);
        REQUIRE(fl2.used() == 0);
        REQUIRE(fl2.empty());

        fl2 = std::move(fl1);
        REQUIRE(fl2.node_size() == sizeof(object) + 4);
        REQUIRE(fl2.capacity_left() == num_objs);
        REQUIRE(fl2.used() == 0);
        REQUIRE_FALSE(fl2.empty());

        std::free(mem);
    }

    SECTION("swap")
    {
        free_list fl1(sizeof(object));
        // node size is increased to 16 so that it is 8-byte aligned
        REQUIRE(fl1.node_size() == sizeof(object) + 4);
        REQUIRE(fl1.capacity_left() == 0);
        REQUIRE(fl1.empty());
        constexpr std::size_t num_objs_1 = 10;
        void* mem1 = std::malloc(fl1.node_size() * num_objs_1);
        fl1.insert(mem1, fl1.node_size() * num_objs_1);
        REQUIRE(fl1.capacity_left() == num_objs_1);
        REQUIRE_FALSE(fl1.empty());

        free_list fl2(sizeof(big_object));
        REQUIRE(fl2.node_size() == sizeof(big_object));
        REQUIRE(fl2.capacity_left() == 0);
        REQUIRE(fl2.empty());
        constexpr std::size_t num_objs_2 = 5;
        void* mem2 = std::malloc(fl2.node_size() * num_objs_2);
        fl2.insert(mem2, fl2.node_size() * num_objs_2);
        REQUIRE(fl2.capacity_left() == num_objs_2);
        REQUIRE_FALSE(fl2.empty());

        std::swap(fl1, fl2);

        // fl1 is now free_list of big_objects
        REQUIRE(fl1.node_size() == sizeof(big_object));
        REQUIRE(fl1.capacity_left() == num_objs_2);
        REQUIRE_FALSE(fl1.empty());

        // fl2 is now free_list of objects
        REQUIRE(fl2.node_size() == sizeof(object) + 4);
        REQUIRE(fl2.capacity_left() == num_objs_1);
        REQUIRE_FALSE(fl2.empty());

        std::free(mem1);
        std::free(mem2);
    }

    SECTION("alloc-dealloc node")
    {
        constexpr std::size_t num_objs = 5;
        void* mem = std::malloc((sizeof(object) + 4) * num_objs);

        free_list fl(sizeof(object), mem, (sizeof(object) + 4) * num_objs);
        REQUIRE(fl.capacity_left() == num_objs);

        // allocate 5 objects and set values
        object* objs[num_objs] = {};
        for (std::size_t i = 0; i < num_objs; ++i) {
            objs[i] = static_cast<object*>(fl.allocate());
            REQUIRE(objs[i] != nullptr);
            objs[i]->a = objs[i]->b = objs[i]->c = i;
            REQUIRE(fl.capacity_left() == num_objs - i - 1);
        }
        REQUIRE(fl.capacity_left() == 0);

        // verify object values
        for (int i = 0; i < (int)num_objs; ++i) {
            REQUIRE(objs[i]->a == i);
            REQUIRE(objs[i]->b == i);
            REQUIRE(objs[i]->c == i);
        }

        // deallocate 2 objs (out of order)
        fl.deallocate(objs[3]);
        REQUIRE(fl.capacity_left() == 1);
        fl.deallocate(objs[1]);
        REQUIRE(fl.capacity_left() == 2);

        objs[3] = static_cast<object*>(fl.allocate());
        objs[3]->a = objs[3]->b = objs[3]->c = 300;
        REQUIRE(fl.capacity_left() == 1);

        objs[1] = static_cast<object*>(fl.allocate());
        objs[1]->a = objs[1]->b = objs[1]->c = 100;
        REQUIRE(fl.capacity_left() == 0);

        // verify object values
        for (int i = 0; i < (int)num_objs; ++i) {
            REQUIRE(objs[i]->a == ((i == 1 || i == 3) ? i * 100 : i));
            REQUIRE(objs[i]->b == ((i == 1 || i == 3) ? i * 100 : i));
            REQUIRE(objs[i]->c == ((i == 1 || i == 3) ? i * 100 : i));
        }

        // deallocate everything
        for (std::size_t i = 0; i < num_objs; ++i) {
            REQUIRE(fl.capacity_left() == i);
            fl.deallocate(objs[i]);
        }

        std::free(mem);
    }

    SECTION("alloc-dealloc array")
    {
        constexpr std::size_t mem_size = sizeof(big_object) * 50;

        free_list fl(sizeof(big_object));
        REQUIRE(fl.node_size() == sizeof(big_object));
        REQUIRE(fl.capacity_left() == 0);
        REQUIRE(fl.empty());

        void* mem = std::malloc(mem_size);
        fl.insert(mem, mem_size);
        REQUIRE(fl.capacity_left() == 50);
        REQUIRE_FALSE(fl.empty());

        // allocate 5 objects and set values
        constexpr std::size_t num_objs = 5;
        big_object* objs = static_cast<big_object*>(fl.allocate(sizeof(big_object) * num_objs));
        for (std::uint64_t i = 0; i < num_objs; ++i)
            objs[i].a = objs[i].b = objs[i].c = i;
        REQUIRE(fl.capacity_left() == 45);

        // verify object values
        for (std::uint64_t i = 0; i < num_objs; ++i) {
            REQUIRE(objs[i].a == i);
            REQUIRE(objs[i].b == i);
            REQUIRE(objs[i].c == i);
        }

        // deallocate array
        fl.deallocate(objs, sizeof(big_object) * num_objs);
        REQUIRE(fl.capacity_left() == 50);

        std::free(mem);
    }
}
